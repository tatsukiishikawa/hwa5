import java.util.*;

import static org.junit.Assert.assertEquals;

/** Tree with two pointers.
 * @since 1.8
 */
public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   private static boolean isOperator(String opcode) {
      if (opcode.equals("+") || opcode.equals("-") || opcode.equals("/") || opcode.equals("*")) return true;
      return false;
   }

   private static boolean isNumeric(String strNum) {
      //https://stackoverflow.com/questions/1102891/how-to-check-if-a-string-is-numeric-in-java
      if (strNum == null) {
         return false;
      }
      return strNum.matches("-?\\d+(\\.\\d+)?");
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      // TODO!!!

      b.append(name);
      if (nextSibling != null) {
         b.append("(");
         b.append(nextSibling);
         b.append(",");
      }
      if (firstChild != null) {
         b.append(firstChild);
         b.append(")");
      }

      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      //https://en.wikipedia.org/wiki/Binary_expression_tree
      String[] arr = pol.split(" ");
      Tnode root = null;
      Stack<Tnode> stack = new Stack<Tnode>();

      int numCounter = 0;
      int opCounter = 0;
      for (String opcode: arr) {
         Tnode node = new Tnode();
         root = node;
         if (isOperator(opcode)) {
            // if element is a operator, generate new tree
            if (stack.size() < 2) throw new IllegalArgumentException("Less number of opcodes found to operate arithmetic operation " + opcode + " .");
            node.firstChild = stack.pop(); // left side
            node.nextSibling = stack.pop(); // right side
            node.name = opcode;
            stack.add(node);
            opCounter++;
         } else if (isNumeric(opcode)) {
            node.name = opcode;
            stack.add(node);
            numCounter++;
         } else {
            if (stack.size() < 2) throw new IllegalArgumentException("Illegal symbol is given. " + opcode);
         }
      }
      if (numCounter - opCounter != 1) {
         throw new IllegalArgumentException("Number of opcode and operator does not match.");
      }
      return root;
   }

   public static void main (String[] param) {
      String rpn = "2 1 - 4 * 6 3 / +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      String r = res.toString().replaceAll("\\s+", "");
      assertEquals ("Tree: " + rpn, "+(*(-(2,1),4),/(6,3))", r);
      // TODO!!! Your tests here
   }
}

